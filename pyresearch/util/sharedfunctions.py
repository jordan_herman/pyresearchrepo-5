#
import pandas as pd
import pyresearch
def fixBeakerBool(input_df):
    # PUBLIC
    output_df = input_df
    for x in output_df.columns:
        if output_df.dtypes[x] == 'bool':
            output_df[x] = output_df[x].astype(str)
    return output_df

def explode_array(in_df, explode_col):
    out_df = pd.concat({k: pd.DataFrame(array) for k, array in in_df.pop(explode_col).items()})
    return out_df

def join_dfs(in_df1, in_df2):
    out_df = in_df2.reset_index(level=1, drop=True).join(in_df1, lsuffix='0').reset_index(drop=True)
    out_df = pyresearch.util.fixBeakerBool(out_df)
    return out_df

# the explode_array join_dfs functions can be used in conjuntion to explode a dataframe column that contains an array, creating a new dataframe without losing any field associations 
# example usage:
# hostaxis hosts resources airasia.com -c -1 -t 2019/01/01
# df1 = prev_hostaxis_df
# df2 = explode_array(df1, 'resources')
# df2 = join_dfs(df1, df2)
# df2
