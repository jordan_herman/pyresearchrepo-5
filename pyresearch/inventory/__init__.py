# Parquet Functions
from collections import OrderedDict
from pyresearch.inventory.sharedfunctions import *
import pandas as pd
import pyresearch

docs_dict = OrderedDict()


#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['checkdfhostnameagainstinventory'] = {'fargs':'parent_df: the dataframe with the field to check\nhostnamefield: the name of the column to with the hostsnames\ndatefilter: If you only wish to check records newer than a certain date, this is the date (Optional set to none to ignore)\ndatefilterfield: This is the field in the dataframe to check the previous datefilter against(Optional, set to none to ignore)\ndebug: turn on extra of output',
               'fret': 'Dictionary of hostnames and lookups',
               'shortdesc': "Lookup a host against inventory to see if it's associated",
               'desc': 'checkdfhostnameagainstinventory(parent_df, hostnamefield, datefilter=None, datefilterfield=None, debug=False)\nInventory Lookup of a dataframe. Provide the df, and a hostname field to lookup.\nOtherfields optional'
               }

docs_dict['get_host_labels'] = {'fargs':'hosts_list: a list of hosts to check inventory for associated workspaces\ndebug: turn on verbose output',
               'fret': 'Dict of hosts with associated workspaces',
               'shortdesc': 'Return associated workspaces based a lists of hosts',
               'desc': 'get_host_labels(hosts_lists, debug=False)\nProvide a list of hosts, return a dictionary of associated workspaces'
               }


docs_dict['workspacenamelookup'] = {'fargs':'wsid - The Workspace ID, with a preceeding w or not (either works here)\ndebug (optional, default: False) - Print extra info',
               'fret': 'String name of the the workspace ID',
               'shortdesc': 'Return a workspace name by workspace ID',
               'desc': 'workspacenamelookup(wsid, debug=False)\nHit the MySQL server, and provide a Workspace Name based on a workspace ID. Both w1234 and 1234 would return the name of workspaceID 1234'
               }


def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)



